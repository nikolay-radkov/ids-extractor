var request = require('superagent');

// FireBase link for creating dynamic link with their REST API
var url = 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyAjpTTodPXAzyX2junAFmZXyvsaHAIeX9A';

var destination = document.getElementById('result');
destination.innerText = 'Here you will see the generated link';

function generateLink() {
  var index = 0;

  var utmCampaign = document.getElementById('utmCampaign').value;
  var utmSource = document.getElementById('utmSource').value;
  var utmMedium = document.getElementById('utmMedium').value;
  var utmTerm = document.getElementById('utmTerm').value;
  var utmContent = document.getElementById('utmContent').value;
  var additionalKey = document.getElementById('additionalKey').value;
  var additionalValue = document.getElementById('additionalValue').value;

  var socialTitle = document.getElementById('socialTitle').value;
  var socialDescription = document.getElementById('socialDescription').value;
  var socialImageLink = document.getElementById('socialImageLink').value;

  var referrer = `utm_source=${utmSource}&utm_medium=${utmMedium}&utm_campaign=${utmCampaign}`;
  var link = "http://www.goseemba.com";
  var hasQueryStrings = false;
  var sign = '?';

  if (additionalKey && additionalValue) {
    link = `${link}?${additionalKey}=${additionalValue}`;
    referrer = `${referrer}&${additionalKey}=${additionalValue}`;
    hasQueryStrings = true;
  }

  if (utmTerm) {
    sign = hasQueryStrings ? '&' : '?';
    link = `${link}${sign}utm_term=${utmTerm}`;
    referrer = `${referrer}&utm_term=${utmTerm}`;
    hasQueryStrings = true;
  }

  if (utmContent) {
    sign = hasQueryStrings ? '&' : '?';
    link = `${link}${sign}utm_content=${utmContent}`;
    referrer = `${referrer}&utm_content=${utmContent}`;
  }

  referrer = encodeURIComponent(referrer);

  var payload = {
    dynamicLinkInfo: {
      dynamicLinkDomain: "gg5y7.app.goo.gl",// SnappBuilder shareable domain
      link,
      androidInfo: {
        androidPackageName: "com.goseemba",
        androidFallbackLink: `https://play.google.com/store/apps/details?id=com.goseemba&referrer=${referrer}`
      },
      navigationInfo: {
        enableForcedRedirect: "true"
      },
      analyticsInfo: {
        googlePlayAnalytics: {
          utmSource,
          utmMedium,
          utmCampaign,
        }
      },
      socialMetaTagInfo: {
      }
    }
  }

  if (socialTitle) {
    payload.dynamicLinkInfo.socialMetaTagInfo.socialTitle = socialTitle;
  }

  if (socialDescription) {
    payload.dynamicLinkInfo.socialMetaTagInfo.socialDescription = socialDescription;
  }

  if (socialImageLink) {
    payload.dynamicLinkInfo.socialMetaTagInfo.socialImageLink = socialImageLink;
  }

  if (utmTerm) {
    payload.dynamicLinkInfo.analyticsInfo.googlePlayAnalytics.utmTerm = utmTerm;
  }

  if (utmContent) {
    payload.dynamicLinkInfo.analyticsInfo.googlePlayAnalytics.utmContent = utmContent;
  }

  sendRequest(payload);
}

function sendRequest(payload) {
  request
    .post(url)
    .send(payload)

    .end(function (err, res) {
      if (err) {
        destination.innerText += `\n ERROR - ${JSON.stringify(err)}\n`;
      } else {
        const result = JSON.parse(res.text);
        destination.innerText += `\n SHORT LINK=${result.shortLink} \n PREVIEW LINK=${result.previewLink}`;
      }
    });
}

var buildButton = document.getElementById('build');
buildButton.addEventListener('click', generateLink);
